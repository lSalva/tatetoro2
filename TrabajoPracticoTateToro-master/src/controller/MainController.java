package controller;

import javax.swing.JButton;

import logic.MainLogic;

import view.MainView;

public class MainController {

	private MainView view;
	private static String turno = "X";

	public MainController(MainView view) {
		this.view = view;
	}
	
	public void InicializarPantalla() {
		view.inicializar();
	}

	public static String getTurno() {return turno;}
	public static void setTurno(String turnoR) {turno = turnoR;}
	
	public static String getGanador() {return MainLogic.getGanador();}
	
	public static int getContTurno() {return MainLogic.getContTurnos();}
	
	public static boolean estadoPartida(JButton[][] matriz) {
		String[][] contenidoDeMatriz = cambioDeMatriz(matriz);
		MainLogic.setMatriz(contenidoDeMatriz);
		return MainLogic.validarPartida();
	}
	public static String[][] cambioDeMatriz(JButton[][] matriz){
		String[][] ret = new String[3][3];
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				ret[i][j] = matriz[i][j].getText();
			}
		}
		return ret;
		
	}

	public static int[] getCoordenadaGanadorPrimero() {
		return MainLogic.botonesGanadores[0];
	}

	public static int[] getCoordenadaGanadorSegundo() {
		return MainLogic.botonesGanadores[1];
	}

	public static int[] getCoordenadaGanadorTercero() {
		return MainLogic.botonesGanadores[2];
	}
}
