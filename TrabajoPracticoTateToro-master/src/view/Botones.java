package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.MainController;

public class Botones {

	private JFrame frame;
	public static JButton[][] listaBotones;

	public Botones(JFrame frame, int nivel) {
		this.frame = frame;
		Botones.listaBotones = new JButton[nivel][nivel];
	}

	public void llenarListaBotones() {
		for (int i = 0; i < listaBotones.length; i++) {
			for (int j = 0; j < listaBotones[0].length; j++) {
				JButton boton = new JButton();
				boton.setFont(new Font("Tahoma",Font.PLAIN,90));
				boton.setText("");
				agregarEventoBoton(boton);
				this.frame.getContentPane().add(boton);
				listaBotones[i][j] = boton;
			}
		}
	}
	
	private void agregarEventoBoton(JButton boton) {
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boton.setText(MainController.getTurno());
				boton.setEnabled(false);
				if(MainController.estadoPartida(listaBotones)) {
					//Pantallita de ganador
					pintarBotonesGanadores();
					desactivarBotones();
					JOptionPane.showMessageDialog(frame, "Felicitaciones has ganado jugador: " + MainController.getGanador());
					return;
				}
				frame.setTitle("Turno de jugador: " + MainController.getTurno() + ", Turno numero: " + MainController.getContTurno());
			}
		});
	}
	
	private void desactivarBotones() {
		for (int i = 0; i < listaBotones.length; i++) {
			for (int j = 0; j < listaBotones.length; j++) {
				listaBotones[i][j].setEnabled(false);
			}
		}
	}
	
	// Pinta los botones ganadores de rojo llamando individualmente por boton a pintarBoton
		private static void pintarBotonesGanadores() {
			pintarBoton(listaBotones[MainController.getCoordenadaGanadorPrimero()[0]][MainController.getCoordenadaGanadorPrimero()[1]]);
			pintarBoton(listaBotones[MainController.getCoordenadaGanadorSegundo()[0]][MainController.getCoordenadaGanadorSegundo()[1]]);
			pintarBoton(listaBotones[MainController.getCoordenadaGanadorTercero()[0]][MainController.getCoordenadaGanadorTercero()[1]]);
		}
		
	// Pinta un solo boton
	private static void pintarBoton(JButton boton) {
		boton.setBackground(Color.RED);
		boton.setContentAreaFilled(false);
		boton.setOpaque(true);
	}
}
