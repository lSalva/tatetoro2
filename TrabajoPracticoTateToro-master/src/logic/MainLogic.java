package logic;

import controller.MainController;

public class MainLogic {

	private static String ganador = " ";
	private static String[][] _matriz;
	public static int[][] botonesGanadores = new int[3][2];
	private static int contTurnos=1;

	public static int getContTurnos() {return contTurnos;}
	public static void setContTurnos(int contTurnos) {MainLogic.contTurnos = contTurnos;}
	
	public static void setGanador(String ganadorR) {MainLogic.ganador = ganadorR;}
	public static String getGanador() {return ganador;}
	
	public static void setMatriz(String[][] matriz) {_matriz = matriz;}
	
	// Cambiar el turno del jugador que esta jugando
	private static void changeTurno() {
		if(MainController.getTurno() == "X") { 
			MainController.setTurno("O");
		}
		else {
			MainController.setTurno("X");
		}
	}
	
	// Validacion de partida
	public static boolean validarPartida() {

		if (validarPorFilas()) {
			return true;
		}

		if (validarPorColumnas()) {
			return true;
		}

		if (validarPorDiagonal()) {
			return true;
		}
		
		if (validarPorTatetoro()) {
			return true;
		}
		
		if (contTurnos==9) {
			setGanador("Nadie");
			return true;
		}
		
		contTurnos++;
		changeTurno();
		return false;
	}

	// validar Por Filas
	private static boolean validarPorFilas() {
		int cont;
		for (cont = 0; cont < 3; ++cont) {
			if (validar(_matriz[cont][0],_matriz[cont][1],_matriz[cont][2])) {
				setBotonesGanadores(cont,0,cont,1,cont,2);
				return true;
			}
		}
		return false;
	}
			
	// validar Por Columnas
	private static boolean validarPorColumnas() {
		int cont;
		for (cont = 0; cont < 3; ++cont) {
			if (validar(_matriz[0][cont],_matriz[1][cont],_matriz[2][cont])) {
				setBotonesGanadores(0,cont,1,cont,2,cont);
				return true;
			}
		}

		return false;
	}

	private static boolean validarPorDiagonal() {
		// validar por diagonal 0,0 hasta 2,2
		if(validar(_matriz[0][0],_matriz[1][1],_matriz[2][2])) {
			setBotonesGanadores(0,0,1,1,2,2);
			return true;
		}
		// validar por diagonal 0,2, hasta 2,0
		else if(validar(_matriz[0][2],_matriz[1][1],_matriz[2][0])) {
			setBotonesGanadores(0,2,1,1,2,0);
			return true;
		}
		return false;
	}
	
	private static boolean validarPorTatetoro() {
		if(validar(_matriz[0][1],_matriz[1][2],_matriz[2][0])) {
			setBotonesGanadores(0,1,1,2,2,0);
			return true;
		}
		else if(validar(_matriz[1][0],_matriz[0][1],_matriz[2][2])) {
			setBotonesGanadores(1,0,0,1,2,2);
			return true;
		}
		else if(validar(_matriz[1][0],_matriz[2][1],_matriz[0][2])) {
			setBotonesGanadores(1,0,2,1,0,2);
			return true;
		}
		else if(validar(_matriz[0][0],_matriz[2][1],_matriz[1][2])) {
			setBotonesGanadores(0,0,2,1,1,2);
			return true;
		}
		return false;
	}
	
	// Recorre y verifica si los botones dados son iguales para ver si el jugador gano
	private static boolean validar(String primero, String segundo, String tercero) {
		if (primero == segundo && segundo == tercero && tercero != "") {
			setGanador(primero);
			return true;
		}
		return false;
	}
	
	private static void setBotonesGanadores(int prim, int seg, int terc, int cuar, int quin, int sex) {
		botonesGanadores[0][0] =prim;
		botonesGanadores[0][1] =seg;
		botonesGanadores[1][0] =terc;
		botonesGanadores[1][1] =cuar;
		botonesGanadores[2][0] =quin;
		botonesGanadores[2][1] =sex;
	}
}
